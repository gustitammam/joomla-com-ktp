-- -----------------------------------------------------
-- Table `#__ktp_cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `#__ktp_cities` (
  `ktp_city_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(50) NOT NULL,
  `asset_id` INT(10) NOT NULL DEFAULT '0',
  `created_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` BIGINT(20) NOT NULL DEFAULT '0',
  `modified_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` BIGINT(20) NOT NULL DEFAULT '0',
  `locked_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` BIGINT(20) NOT NULL DEFAULT '0',
  `ordering` INT(10) NOT NULL DEFAULT '0',
  `enabled` TINYINT(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ktp_city_id`)
) DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `#__ktp_items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `#__ktp_items` (
  `ktp_item_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nik` VARCHAR(16) NOT NULL,
  `slug` VARCHAR(50) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `place_of_birth_id` BIGINT(20) UNSIGNED NOT NULL,
  `date_of_birth` DATETIME NOT NULL,
  `occupation` VARCHAR(100) NOT NULL,
  `image` VARCHAR(500),
  `asset_id` INT(10) NOT NULL DEFAULT '0',
  `created_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` BIGINT(20) NOT NULL DEFAULT '0',
  `modified_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` BIGINT(20) NOT NULL DEFAULT '0',
  `locked_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` BIGINT(20) NOT NULL DEFAULT '0',
  `ordering` INT(10) NOT NULL DEFAULT '0',
  `enabled` TINYINT(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ktp_item_id`),
  INDEX `fk_#__ktp_items_#__ktp_cities_idx` (`place_of_birth_id` ASC),
  CONSTRAINT `fk_#__ktp_items_#__ktp_cities`
    FOREIGN KEY (`place_of_birth_id`)
    REFERENCES `#__ktp_cities` (`ktp_city_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) DEFAULT CHARSET=utf8;
