# com_ktp
Contoh component Joomla dengan menggunakan FOF Framework versi 3.0.5.
Component ini merupakan CRUD yang mengolah data-data yang ada di KTP.

Sebelum menginstall component ini, silahkan download dan install terlebih dahulu FOF versi 3.0.5 [disini](https://www.akeebabackup.com/download/fof3/3-0-5.html).

# Screenshot
![screenshot](DOCS/screenshot_com_ktp.png)